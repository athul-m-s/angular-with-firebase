// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA7fvKHMKbZRUynC2wFGaNV5l1bR07u9Xs",
    authDomain: "shopo-336f3.firebaseapp.com",
    databaseURL: "https://shopo-336f3.firebaseio.com",
    projectId: "shopo-336f3",
    storageBucket: "shopo-336f3.appspot.com",
    messagingSenderId: "564268579324",
    appId: "1:564268579324:web:3dab1187eabd24ffc9da79",
    measurementId: "G-BQ57FLZ1EX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
