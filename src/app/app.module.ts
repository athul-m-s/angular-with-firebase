import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminOrdersComponent } from './componenets/admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './componenets/admin/admin-products/admin-products.component';
import { ProductFormComponent } from './componenets/admin/product-form/product-form.component';
import { CheckOutComponent } from './componenets/check-out/check-out.component';
import { HomeComponent } from './componenets/home/home.component';
import { LoginComponent } from './componenets/login/login.component';
import { MyOrdersComponent } from './componenets/my-orders/my-orders.component';
import { NavbarComponent } from './componenets/navbar/navbar.component';
import { OrderSuccessComponent } from './componenets/order-success/order-success.component';
import { ProductsComponent } from './componenets/products/products.component';
import { ShoppingCartComponent } from './componenets/shopping-cart/shopping-cart.component';
import { MatComponenetsModule } from './modules/mat-componenets/mat-componenets.module';
import { AuthService } from './services/auth-service/auth.service';
import { AuthGuardService } from './services/authguard/auth-guard.service';
import { AdminAuthGuardService } from './services/adminAuthGuard/admin-auth-guard.service';
import { UserService } from './services/user-service/user.service';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CategoryService } from './services/categoryService/category.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from './services/product-service/product.service';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    AdminOrdersComponent,
    AdminProductsComponent,
    ProductFormComponent,
    CheckOutComponent,
    HomeComponent,
    LoginComponent,
    MyOrdersComponent,
    NavbarComponent,
    OrderSuccessComponent,
    ProductsComponent,
    ShoppingCartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    MatComponenetsModule,


    RouterModule.forRoot([

      { path: "", component: HomeComponent },
      { path: "products", component: ProductsComponent },
      { path: "shopping-cart", component: ShoppingCartComponent },
      { path: "login", component: LoginComponent },

      { path: "checkout", component: CheckOutComponent, canActivate: [AuthGuardService] },
      { path: "my/orders", component: MyOrdersComponent, canActivate: [AuthGuardService] },
      { path: "order-success", component: OrderSuccessComponent, canActivate: [AuthGuardService] },

      { path: "admin/product/new", component: ProductFormComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
      { path: "admin/products/:id", component: ProductFormComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
      { path: "admin/products", component: AdminProductsComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
      { path: "admin/orders", component: AdminOrdersComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
    ])
  ],
  providers: [
    AuthService,
    AuthGuardService,
    UserService,
    AdminAuthGuardService,
    CategoryService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
