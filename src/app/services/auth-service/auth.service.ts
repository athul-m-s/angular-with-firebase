import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRoute, Router } from "@angular/router";
import firebase from 'firebase/app';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AppUser } from 'src/app/models/app-user';
import { UserService } from '../user-service/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user$: Observable<firebase.User>
  public retUrl: string;

  constructor(private afAuth: AngularFireAuth, public router: Router, public ngZone: NgZone, private route: ActivatedRoute, private userService: UserService) {
    this.user$ = this.afAuth.authState;
  }

  login() {
    this.retUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.SigninWithGoogle();
  }

  private SigninWithGoogle() {
    return this.OAuthProvider(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        this.saveUser()
      }).catch(error => {
        console.log(error)
      });
  }

  private OAuthProvider(provider: any) {
    return this.afAuth.signInWithPopup(provider)
      .then((res) => {
        this.ngZone.run(() => {
          this.router.navigate([this.retUrl || '/']);
        })
      }).catch((error) => {
        window.alert(error)
      })
  }

  logout() {
    return this.afAuth.signOut().then(() => {
      this.router.navigate(['/']);
    })
  }

  saveUser() {
    this.user$.subscribe(x => {
      this.userService.save(x);
    })
  }

  get appUser$(): Observable<AppUser> {
    return this.user$.pipe(
      switchMap(user => {
        if (user)
          return this.userService.get(user.uid);
        else
          return of(null);
      })
    );
  }

}
