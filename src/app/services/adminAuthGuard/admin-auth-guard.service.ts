import { Injectable } from '@angular/core';
import { CanActivate, } from '@angular/router';
import { AuthService } from '../auth-service/auth.service';
import { map, } from 'rxjs/operators';
import { Observable, } from 'rxjs';
import { UserService } from '../user-service/user.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private userService: UserService) { }

  canActivate(): Observable<boolean> {
    return this.auth.appUser$.pipe(
      map(appUser => {
        return appUser.isAdmin;
      }));
  }
}
