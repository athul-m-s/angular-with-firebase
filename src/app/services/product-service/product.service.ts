import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  studentsRef: AngularFireList<any>;

  constructor(private db: AngularFireDatabase) { }

  careateProduct(product) {
    this.db.list('/products').push(product);
  }

  getAll() {
    this.studentsRef = this.db.list('products');
    return this.studentsRef;
  }

  get(productId): Observable<any> {
    return this.db.object<any>('/products/' + productId)
      .valueChanges()
      .pipe(take(1));
  }

  update(id, product) {
    return this.db.object('/products/' + id).update(product);
  }

  delete(id) {
    this.db.object('/products/' + id).remove()
  }
}
