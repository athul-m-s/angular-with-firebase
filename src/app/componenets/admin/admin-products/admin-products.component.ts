import { JsonPipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product-service/product.service';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit, OnDestroy {

  subscription: Subscription
  products: any[];
  filteredProducts: any[];

  constructor(private productService: ProductService) {

  }

  ngOnInit(): void {
    this.subscription = this.productService.getAll().snapshotChanges().subscribe(data => {
      this.products = [];
      data.forEach(item => {
        let a = item.payload.toJSON();
        a['key'] = item.key;
        this.products.push(a);
      })
      this.filteredProducts = this.products;
    })
  }

  filter(query: string) {
    if (query) {
      this.filteredProducts = this.products.filter(p => p.title.toLowerCase().includes(query.toLowerCase()));
    }
    else {
      this.filteredProducts = this.products;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
