import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'src/app/services/categoryService/category.service';
import { ProductService } from 'src/app/services/product-service/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})

export class ProductFormComponent implements OnInit {

  categories$;
  categoryForm: FormGroup;
  product = {} as Product;
  id;

  constructor(private categoryService: CategoryService, private fb: FormBuilder,
    private productService: ProductService, private route: ActivatedRoute, private router: Router) {
    this.createForm()
    this.categories$ = this.categoryService.getCategories();

    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.productService.get(this.id)
        .subscribe(p => {
          this.product = p;
          this.setFormValues();
        });
    }
  }

  ngOnInit(): void {

  }

  createForm() {
    this.categoryForm = this.fb.group({
      title: ["", [Validators.required]],
      price: ["", Validators.compose([Validators.required, Validators.min(1)])],
      category: ["", Validators.required],
      imageUrl: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
          )
        ])
      ]
    });
  }

  saveForm(form: FormGroup, formDirective: FormGroupDirective) {
    if (this.categoryForm.invalid) {
      return;
    }

    if (this.id) {
      this.productService.update(this.id, this.categoryForm.value);
      this.router.navigate(['/admin/products']);
    }
    else {
      this.productService.careateProduct(this.categoryForm.value);
      formDirective.resetForm()
      form.reset();
    }
  }

  get form() {
    return this.categoryForm;
  }

  setFormValues() {
    if (this.product) {
      this.categoryForm.get('title').setValue(this.product.title);
      this.categoryForm.get('price').setValue(this.product.price);
      this.categoryForm.get('category').setValue(this.product.category);
      this.categoryForm.get('imageUrl').setValue(this.product.imageUrl);
    }
  }

  delete() {
    if (confirm("Are you sure ?")) {
      this.productService.delete(this.id);
      this.router.navigate(['/admin/products']);
    }
  }

}


export interface Product {
  title: string;
  price: number;
  category: string;
  imageUrl: string
}
