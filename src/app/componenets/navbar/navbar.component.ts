import { Component, OnInit } from '@angular/core';
import { AppUser } from 'src/app/models/app-user';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {

  public appUser: AppUser;

  constructor(private auth: AuthService) {
    this.auth.appUser$.subscribe(x => this.appUser = x);
  }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logout();
  }

}
